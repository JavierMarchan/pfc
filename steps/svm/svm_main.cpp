#include "svm.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <armadillo>
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

using namespace std;

void read_problem(vector<vector<double> >); 
void stablish_parameters();
struct svm_model *model;     /* SVM model*/
struct svm_problem problem; /* describe the problem*/
struct svm_parameter param; /* */
struct svm_node *x_space;

int main()
{
  arma::Col<size_t> labels;
  const char *error_message;
  
  /* number of training data*/
  int l;
  /*array containing their target values*/
  double *y;
  /*array of pointers, each of which points to a sparse representation (
   array of svm_node) of one training vector*/
  struct svm_node **x;
  char model_file_name[1024] = "data/model";

  stablish_parameters();
  model = svm_train(&problem, &param);
  if(svm_save_model(model_file_name, model)){
//     fprintf(stderr, "can't save model to file %s\n", model_file_name);
//     exit(1);
  }
  svm_free_and_destroy_model(&model);
  svm_destroy_param(&param);
  free(problem.x);
  free(problem.y);
  
  
  cout << "Hello, SVM" << endl;
}

/* This function stablish the parameters used to obtain the model.
 * See README to learn about the parameters*/
void stablish_parameters()
{
	/* Use default values library*/
	param.svm_type = C_SVC;
	param.kernel_type = RBF;
	param.degree = 3;
	param.gamma = 0;	// 1/num_features
	param.coef0 = 0;
	param.nu = 0.5;
	param.cache_size = 100;
	param.C = 1;
	param.eps = 1e-3;
	param.p = 0.1;
	param.shrinking = 1;
	param.probability = 0;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;

}


/* This function creates the problem from the previous step histogram
 It is called problem to the structure used to train the svm*/
void read_problem(vector< vector< double > > histogram, arma::Col<size_t> labels)
{
  /* numbers of elements of each training vector. All training
   * vectors have the same size.
   * (We know that now will be 2 because the clustering
   * step make two clusters)*/
  vector<double> training_vector = histogram.front();
  long unsigned int elements = training_vector.size();
  
  /* Number of training data*/
  problem.l = (int)histogram.size();
  /**/
  problem.y = Malloc(double, problem.l);
  problem.x = Malloc(struct svm_node *, problem.l);
  /* In each training vector we have n=elements pointers plus one (-1 index, see README)*/
  x_space   = Malloc(struct svm_node, ((int)elements + 1) * problem.l);
  
  //   Now create the array of pointers to a sparse representation of 
  //   training vectors
  int j=0;
  for (int i=0; i<problem.l; i++){
      problem.x[i] = &x_space[j];
      problem.y[i] = (int)labels(i);
      
      training_vector = histogram.at(i);
      double value;
      for (unsigned int n=1; n <= elements; n++){
	x_space[j].index = n;
	value = training_vector.at(n-1);
	x_space[j].value = value;
	++j;
      }
      x_space[++j].index = -1;      
  }
  

}
