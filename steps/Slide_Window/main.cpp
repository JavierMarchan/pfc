#include <iostream>
#include <vector>
#include <string>
#include <string.h>
#include <fstream>
#include <dirent.h>
#include <unistd.h>
#include <math.h>
#include "SlideWindow.h"

using namespace std;

typedef double T;
vector<T> read_signal_from_file(string dir_path);


void print(vector<T> v){
	
	for (auto &i : v){
		std::cout << i << std::endl;
	}
	    
}

int main(int argc, char** argv){
	
	vector<T> window;
	
	//***//vector<T> signal = read_signal_from_file("../data/Aset/");
	vector<T> signal(100);
	for (int i = 0; i < 100; i++ ) signal[i] = i+1;	
	
	//***//SlideWindow sw(signal, 4, 1, 173.61);
	SlideWindow sw(signal, 2, 1, 10);  /*A 100 element vector sampled to 10 Hz => 10 elements each second, so each
	 									 window will have 20 elements (10 of 20 are overlap) */
	
	while (!(window=sw.get_next_window()).empty()){
		cout << "VENTANA: " << sw.get_current_window() << endl;
		cout << "muestras: " << window.size() << endl;
		print(window);
	}
	
	
}

std::vector< T > read_signal_from_file(string dir_data)
{

  std::vector<T> signal;

  DIR *dir;
  dirent *pdir;
  vector<string> files;
  
  dir = opendir(dir_data.c_str());
  
  while ((pdir = readdir(dir))){
    if ( (strcmp(pdir->d_name, ".")) != 0 && (strcmp(pdir->d_name,"..")) != 0){
      files.push_back(pdir->d_name);
    }
  }
  
  closedir(dir);
  
  ifstream data;
  T value;
  
  for (unsigned int i=0; i<files.size(); i++){
    string path_file = dir_data;
    path_file.append(files[i]);
    data.open(path_file.c_str());
    cout << "opened " << path_file << endl;
    while(data >> value){
      signal.push_back(value);
    }

    data.close();
  }
  
  return signal;
}