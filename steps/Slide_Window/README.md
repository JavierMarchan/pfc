=============================================================================

COMPILAR Y EJECUTAR
------------------------

En el directorio Release:

> make -k all

>./prueba_slide_window

=============================================================================

SlideWindow.h
SlideWindow.cpp  

Implementan un algoritmo para hacer una ventana deslizante sobre un vector de datos

=============================================================================

main.cpp

Muestra un ejemplo de uso

=============================================================================

data/

Algunos datos de ejemplo para probar el código

=============================================================================
