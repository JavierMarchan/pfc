#-------------------------------------------------
#
# Project created by QtCreator 2014-02-18T18:49:01
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = get_chanels
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../lib/release/ -ledk
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../lib/debug/ -ledk
else:unix: LIBS += -L$$PWD/../lib/ -ledk

INCLUDEPATH += $$PWD/../
DEPENDPATH += $$PWD/../

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../myexample/lib/release/ -ledk_utils_linux
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../myexample/lib/debug/ -ledk_utils_linux
else:unix: LIBS += -L$$PWD/../myexample/lib/ -ledk_utils_linux

INCLUDEPATH += $$PWD/../myexample
DEPENDPATH += $$PWD/../myexample
