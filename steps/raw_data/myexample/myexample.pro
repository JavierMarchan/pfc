#-------------------------------------------------
#
# Project created by QtCreator 2014-02-18T17:29:58
#
#-------------------------------------------------


TARGET = myexample
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

unix: LIBS += -L$$PWD/lib/ -ledk

INCLUDEPATH += $$PWD/lib
DEPENDPATH += $$PWD/lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/release/ -ledk_utils_linux
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/debug/ -ledk_utils_linux
else:unix: LIBS += -L$$PWD/lib/ -ledk_utils_linux

INCLUDEPATH += $$PWD/lib
DEPENDPATH += $$PWD/lib
