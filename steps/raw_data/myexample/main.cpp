#include <iostream>
#include <fstream>
//#include <conio.h>
#include <sstream>
//#include <windows.h>
#include <map>
#include <sys/select.h>
#include "EmoStateDLL.h"
#include "edk.h"
#include "edkErrorCode.h"
#include "unistd.h"
#include <stdlib.h>

int _kbhit(void)
{
  struct timeval tv;
  fd_set read_fd;

  /* Do not wait at all, not even a microsecond */
  tv.tv_sec=0;
  tv.tv_usec=0;

  /* Must be done first to initialize read_fd */
  FD_ZERO(&read_fd);

  /* Makes select() ask if input is ready:
   * 0 is the file descriptor for stdin    */
  FD_SET(0,&read_fd);

  /* The first parameter is the number of the
   * largest file descriptor to check + 1. */
  if(select(1, &read_fd,NULL, /*No writes*/NULL, /*No exceptions*/&tv) == -1)
    return 0;  /* An error occured */

  /*  read_fd now holds a bit map of files that are
   * readable. We test the entry for the standard
   * input (file 0). */

if(FD_ISSET(0,&read_fd))
    /* Character pending on stdin */
    return 1;

  /* no characters were pending */
  return 0;
}

EE_DataChannel_t targetChannelList[] = {
        ED_COUNTER,
        ED_AF3, ED_F7, ED_F3, ED_FC5, ED_T7,
        ED_P7, ED_O1, ED_O2, ED_P8, ED_T8,
        ED_FC6, ED_F4, ED_F8, ED_AF4, ED_GYROX, ED_GYROY, ED_TIMESTAMP,
        ED_FUNC_ID, ED_FUNC_VALUE, ED_MARKER, ED_SYNC_SIGNAL
    };

const char header[] = "COUNTER,AF3,F7,F3, FC5, T7, P7, O1, O2,P8"
                      ", T8, FC6, F4,F8, AF4,GYROX, GYROY, TIMESTAMP, "
                      "FUNC_ID, FUNC_VALUE, MARKER, SYNC_SIGNAL,";

int main(int argc, char *argv[])
{
    EmoEngineEventHandle eEvent	= EE_EmoEngineEventCreate();
    EmoStateHandle eState				= EE_EmoStateCreate();
    unsigned int userID					= 0;
    const unsigned short composerPort	= 1726;
        float secs							= 0;
    unsigned int datarate				= 0;
    bool readytocollect					= false;
    int option							= 0;
    int state							= 0;
    std::cout << "Hello, World" << std::endl;

    std::string input;
    try {

        if (argc != 2) {
                    std::cout<< "Please supply the log file name.\nUsage: EEGLogger [log_file_name].";
        }

        std::cout << "===================================================================" << std::endl;
        std::cout << "Example to show how to log EEG Data from EmoEngine/EmoComposer."	   << std::endl;
        std::cout << "===================================================================" << std::endl;
        std::cout << "Press '1' to start and connect to the EmoEngine                    " << std::endl;
        std::cout << "Press '2' to connect to the EmoComposer                            " << std::endl;
        std::cout << ">> ";

        /*
        std::getline(std::cin, input, '\n');
        option = atoi(input.c_str());
                std::cout<< option;
                */
        option = 1;
        switch (option) {
            case 1:
            {
                if (EE_EngineConnect() != EDK_OK) {
                                   std::cout <<"Emotiv Engine start up failed";
                }
                                else
                                {
                                    std::cout<<"Emotiv Engine start up sucssessful.";
                                }
                break;
            }
            case 2:
            {
                std::cout << "Target IP of EmoComposer? [127.0.0.1] ";
                std::getline(std::cin, input, '\n');

                if (input.empty()) {
                    input = std::string("127.0.0.1");
                }

                if (EE_EngineRemoteConnect(input.c_str(), composerPort) != EDK_OK) {
                    std::string errMsg = "Cannot connect to EmoComposer on [" + input + "]";
                                        std::cout<< errMsg.c_str();
                }
                break;
            }
            default:
                            std::cout<< "Invalid option...";
                break;
        }


        std::cout << "Start receiving EEG Data! Press any key to stop logging...\n" << std::endl;
        //std::ofstream ofs(argv[1],std::ios::trunc);
        //ofs << header << std::endl;

                DataHandle hData = EE_DataCreate();
        EE_DataSetBufferSizeInSec(secs);

        std::cout << "Buffer size in secs:" << secs << std::endl;

                while (!_kbhit()) {
               // for (int i=1;i++;i=1000){
            state = EE_EngineGetNextEvent(eEvent);

            if (state == EDK_OK) {

                EE_Event_t eventType = EE_EmoEngineEventGetType(eEvent);
                EE_EmoEngineEventGetUserId(eEvent, &userID);

                // Log the EmoState if it has been updated
                if (eventType == EE_UserAdded) {
                    std::cout << "User added";
                    EE_DataAcquisitionEnable(userID,true);
                    readytocollect = true;
                }
            }

            if (readytocollect) {

                        EE_DataUpdateHandle(0, hData);

                        unsigned int nSamplesTaken=0;
                                                float secs1=0;
                        EE_DataGetNumberOfSample(hData,&nSamplesTaken);
                                                EE_DataSetBufferSizeInSec(secs1);
                                                std::cout << "Updated " << nSamplesTaken << secs1 << std::endl;

                        if (nSamplesTaken != 0) {

                            double* data = new double[nSamplesTaken];
                            for (int sampleIdx=0 ; sampleIdx<(int)nSamplesTaken ; ++ sampleIdx) {
                                for (int i = 0 ; i<sizeof(targetChannelList)/sizeof(EE_DataChannel_t) ; i++) {

                                    EE_DataGet(hData, targetChannelList[i], data, nSamplesTaken);
                                    //ofs << data[sampleIdx] << ",";
                                    std::cout << data[sampleIdx] << ",";
                                }
                                //ofs << std::endl;
                                std::cout << std::endl;
                            }
                            delete[] data;
                        }


            }

                        sleep(0.01);
        }

        //ofs.close();
        EE_DataFree(hData);

    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        std::cout << "Press any key to exit..." << std::endl;
        getchar();
    }

    EE_EngineDisconnect();
    EE_EmoStateFree(eState);
    EE_EmoEngineEventFree(eEvent);

    return 0;

}
