#include <mlpack/methods/kmeans/kmeans.hpp>
// #include </usr/include/armadillo_bits/fn_trans.hpp>
#include <string>

using namespace mlpack::kmeans;
using namespace std;

int main(int argc, char** argv){
	
	const string input_file = "data/data.csv";
	const string output_file= "data/assignments.csv";
	const string centr_file = "data/centroids.csv";
	const size_t clusters = 4;
	
	//The dataset we are clustering
	arma::mat data;
	mlpack::data::Load(input_file, data, true);

	//The assignments and centroids will be stored in this vector
	arma::Col<size_t> assignments;
	arma::mat centroids;
	
	//Initialize with the default arguments
	KMeans<> k;
	k.Cluster(data, clusters, assignments, centroids);
	
	//Save only the labels
	arma::Mat<size_t> output = trans(assignments);
	mlpack::data::Save(output_file, output);
	
	//Save the centroids
	mlpack::data::Save(centr_file, centroids);
	
	// Create armadillo matrix manually instead loading from file
	
// 	arma::mat prueba;
// 	prueba << 1 << 0 << arma::endr
// 	       << 4 << 0 << arma::endr;
// 	cout << " prueba.print();" << endl;
// 	prueba.print();
// 	
// 	prueba = trans(prueba);
// 	prueba.print();
// 	
	
		
  
}