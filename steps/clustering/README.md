Para hace el clustering se ha elegido la librería mlpack (http://www.mlpack.org/index.html)
Es una librería para C++ de Machine Learning que enfatiza la escalabilidad, la
velocidad y la facilidad de uso. 

La instalación (aquí http://www.mlpack.org/trac/doxygen/build.html se comentan algunos pasos) puede presentar algunos problemas debido a sus dependencias. Los pasos mostrados a continuación se han probado sobre un Ubuntu 12

1) Instalar alguans liberías de las que depende mlpack

> apt-get install libboost-math-dev libboost-program-options-dev
  libboost-random-dev libboost-test-dev libxml2-dev

Ahora hay que instalar armadillo, pero antes hay que satisfacer algunas dependencias

2) Instalar BLAS y LAPACK
> sudo apt-get install liblapack-dev

3) Instalar ATLAS
> sudo apt-get install libatlas-dev

4) Instalar ARPACK	
> sudo apt-get install libarpack2 libarpack2-dev

Hasta aquí se han instalado las dependecias para instalar Armadillo

5) Descargar armadillo y descomprimirlo

6) En el directorio:
> cmake .
> make
> sudo make install

Ahora se puede instalar MLPACK

7) Descargar y descomprimirlo

8) En el directorio:
> mkdir build;
>cd build;
cmake ../
sudo make install

>ldconfig
