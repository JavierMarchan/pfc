clear all
data_a= load('/home/mjsantof/repo/TFG/pfc.javier.marchan/pfc/steps/clustering/bin/data/data_Aset.csv')
[ids_aset ctrs_aset]=kmeans(data_a, 2)
data_e= load('/home/mjsantof/repo/TFG/pfc.javier.marchan/pfc/steps/clustering/bin/data/data_Eset.csv')
[ids_eset ctrs_eset]=kmeans(data_e, 2)

ctrs = [ctrs_aset; ctrs_eset]
data = [data_a; data_e]
dist=[]

%% La función pdist necesita montar un array con el valor de cada elemento de set que se va a medir 
%% y los centroides de los dos clusters de cada grupo (A y E set). 
%% Una vez montado el array en vectorToComputeEuclidean podemos llamar a pdist para que calcule la distancia
% for i=1:size(data)
%     vectorToComputeEuclidean = [data(i,:); ctrs_aset(1,:); ctrs_aset(2,:); ctrs_eset(1,:); ctrs_eset(2,:)]
%     dist= [dist; pdist(vectorToComputeEuclidean)]
% end
% 
%% El histograma se obtiene del vector que ha computado las distancias eucledianas, pero sólo fijándonos en las 4
%% primeras columnas, porque las otras 6 son de mirar la distancia consigo mismo. No varía

%histogram = dist(:,1:4)
%% si queremos testear con los datos que devuelva el codigo C, descomentar la siguiente linea
%% habra sido necesario devolver la salida del k-means a un archivo llamado histogram (./k-means &> histogram)
histogram = load('/home/mjsantof/repo/TFG/pfc.javier.marchan/pfc/steps/clustering/bin/histogram')
histogram_A =  histogram(1:1183,:)
histogram_E = histogram(1183:2366,:)

%% group_A y group_B son las etiquetas de los grupos. 1 para el A set (elementos de la fila 1 a la 1183) y 2 para el E 
%% elementos de la fila 1184 a la 2366
group_A = ones(1183,1);
group_E = ones(1183,1)+group_A;
group = [group_A; group_E]
model = svmtrain(histogram,group);   
resultOfTesting_A =svmclassify(model,histogram_A);
resultOfTesting_E =svmclassify(model,histogram_E);
accuracy_A=sum(1==resultOfTesting_A)/length(resultOfTesting_A)
accuracy_E=sum(2==resultOfTesting_E)/length(resultOfTesting_E)

%% vamos a montar ahora el archivo para entrenar con el svm <label> <index1>:<value1> <index2>:<value2> ...
fileID = fopen('hisToTrain','w');
for i=1:size(histogram)
    fprintf(fileID,'%d 1:%f 2:%f 3:%f 4:%f\n',group(i), histogram(i, 1:4));
end
fclose(fileID);

fileID = fopen('hisToTestA','w');
for i=1:size(histogram_A)
    fprintf(fileID,'1 1:%f 2:%f 3:%f 4:%f\n', histogram_A(i, 1:4));
end

fileID = fopen('hisToTestE','w');
for i=1:size(histogram_E)
    fprintf(fileID,'2 1:%f 2:%f 3:%f 4:%f\n', histogram_E(i, 1:4));
end