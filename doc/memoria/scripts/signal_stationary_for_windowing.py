#code modified from http://glowingpython.blogspot.com.es/2011/08/how-to-plot-frequency-spectrum-with.html

from numpy import sin, cos, linspace, pi
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import fft, arange

import matplotlib.pyplot as plt

x = [1,2,3,4,7,10,11,12,20,25,30]
y = [7,5,1,1,4,6,5,4,1,7,7]

plt.scatter(x, y)
plt.xticks(arange(min(x), max(x)+1, 1.0))
plt.yticks(arange(min(y)-1, max(y)+1, 1.0))

cenx = [2.5, 10, 25]
ceny = [3.5, 4.75, 5]

plt.scatter(cenx, ceny, color='red')
plt.show()