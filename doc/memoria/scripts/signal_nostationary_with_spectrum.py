#code modified from http://glowingpython.blogspot.com.es/2011/08/how-to-plot-frequency-spectrum-with.html

from numpy import sin, cos, linspace, pi, concatenate
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import fft, arange, hstack
import matplotlib.pyplot as plt
def plotSpectrum(y, Fs):
 """
 Plots a Single-Sided Amplitude Spectrum of y(t)
 """
 n = len(y) # length of the signal
 k = arange(n)
 T = n/Fs
 frq = k/T # two sides frequency range
 frq = frq[range(n/5)] # one side frequency range

 Y = fft(y)/n # fft computing and normalization
 Y = Y[range(n/5)]
 
 plot(frq,abs(Y),'r') # plotting the spectrum
 xlabel('Freq (Hz)')
 ylabel('|Y(freq)|')
 
 tickpos=arange(0,200,10)
 ticklabels=[]
 for point in tickpos:
	 ticklabels.append(point)
	 
 plt.xticks(tickpos, ticklabels)

#main
Fs = 1000.0;  # sampling rate
Ts = 1.0/Fs; # sampling interval
t = arange(0,1,Ts) # time vector

ff = 10;   # frequency of the signal
w= 2*pi
y1 = sin(w*ff*t[:400])
y2 = sin(w*2*ff*t[400:800]) 
y3 = sin(w*5*ff*t[800:1000])
y = hstack((y1,y2,y3))
subplot(2,1,1)
plot(t, y)
ylabel('Amplitud')
xlabel('Tiempo (s)')
subplot(2,1,2)
plotSpectrum(y,Fs)
show()
