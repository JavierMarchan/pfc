\chapter{Arquitectura}
\label{chap:arquitectura}

\drop{E}{n}  este  capítulo se  detalla  la  arquitectura del  sistema
desarrollado.  Se  definirá  de  manera  abstracta  cada  uno  de  los
componentes y  la relación  entre ellos. En  todo momento,  desde el
diseño  preliminar hasta  la  implementación, se  ha  tenido en  mente
favorecer la  reutilización, pero  sobre todo la  ampliación posterior
del sistema  y la fácil  modificabilidad ante cualquier cambio  en los
requisitos.

El  algoritmo consta de  dos partes  claramente diferenciadas.  Por un
lado, la parte dedicada a  entrenar (\emph{training}) el sistema y por
otra  la  parte  dedicada   a  la  predicción  (\emph{predict}),  cuya
precisión  se da en  función de  la configuración  de los  módulos que
intervienen en el entrenamiento del sistema.

Este capítulo y el anterior están estrechamente relacionados.

\section{Arquitectura \emph{pipes and filters}}
% \emph{«Architecture is the fundamental organization of a system embodied in its components, their relationships
% to each other, and to the environment, and the principles guiding its design and evolution».} [IEEE 1471]

La definición de la arquitectura no fue una tarea específica
sino un resultado que se iba obteniendo con el avance del sistema.

La complejidad del software y su tamaño, así como la reutilización de estructuras
utilizadas en problemas similares son factores que motivan el uso de una arquitectura
software que incluya la descripción de los componentes que forman el sistema, de la interacción 
entre ellos y de los patrones que guían su composición. El objetivo es obtener un diseño 
preliminar de alto nivel y una visión general de la organización del sistema.

Los estilos arquitectónicos generalmente se clasifican en: 
\begin{itemize}
 \item Sistemas basados en flujos de datos
 \item Sistemas Call/Return
 \item Componentes independientes
 \item Máquinas virtuales
 \item Sistemas centrados en datos
\end{itemize}

El primero de ellos está caracterizado por tratar al sistema como un
conjunto de transformaciones
sobre flujos de entrada de datos y tiene como ejemplo 
típico la arquitectura \emph{pipe and filter}.

En la arquitectura \emph{pipe and filter} cada componente lee un flujo de datos de su entrada y produce
un flujo de datos a su salida aplicando una transformación local. Estos componentes se denominan
\emph{filters}. Los componentes denominados \emph{pipes} son los encargados de conectar los flujos de datos,
desde la salida de un filtro a la entrada de otro.

Una característica importante de este estilo es que los filtros deben ser entidades independientes y no
deben compartir su estado con otros filtros.
Los mejores ejemplos de arquitecturas \emph{pipe and filter} son
los programas escritos en Unix shell, 
los compiladores, y los que se dan en el dominio 
del procesamiento de la señal, entre otros.

En este caso, se ha optado por una especialización de la 
arquitectura \emph{pipe and filter}
que restringe la topología a una secuencia lineal, cuyo esquema
se puede apreciar en la Figura \ref{fig:stages}. Algunas 
de las ventajas que aporta es la facilidad de mantenimiento, crecimiento y de análisis de 
rendimiento.

\begin{figure}[b] % escoger H|h|t|b
\centering
\includegraphics[width=\textwidth]{/compiler.png}
\caption{Modelo tradicional de un compilador. Imagen obtenida de ~\cite{compiler}}
\label{fig:compiler}
\end{figure}

\section{Descripción general}

A continuación se describen los diferentes módulos o elementos que conforman el sistema a alto nivel:
\begin{itemize}
 \item \textbf{Módulo de obtención de señal}: Se encarga de leer una señal de EEG.
 \begin{itemize}
  \item \textbf{Submódulo Leer desde fichero}: Obtiene una señal desde uno o varios ficheros.
  \item \textbf{Submódulo Leer desde EEG}: Obtiene una señal desde un dispositivo de EEG.
 \end{itemize}

 \item \textbf{Módulo de Bag of Words}: Se encarga de realizar las transformaciones a la señal necesarias 
 y que ya se han descrito. Es el módulo principal. A su vez, se puede descomponer en:
 \begin{itemize}
  \item \textbf{Submódulo de DWT}: Aplica una Discrete Wavelet Transform a una señal.
  \item \textbf{Submódulo de Clustering}: Hace clustering sobre un conjunto de vectores que describen una señal.
  \item \textbf{Submódulo de Clasificación}: 
  \begin{itemize}
   \item Entrena el clasificador con un conjunto de vectores que describen una señal.
   \item Clasifica un conjunto de vectores que describen una señal.
  \end{itemize}

 \end{itemize}
 
\end{itemize}

La  Figura  \ref{fig:modulos3}   muestra  los  distintos  módulos  que
conforman el sistema.

\begin{figure}[h] % escoger H|h|t|b
\centering
\includegraphics[width=0.8\textwidth]{/modulos4.png}
\caption{Módulos}
\label{fig:modulos3}
\end{figure}


% \newpage
% ESTO TENDRÁ QUE IR EN OTRA SECCIÓN. «DISEÑO» O SIMILAR

% Figura \ref{fig:class_diagram}
% Figura \hyperref[fig:class_diagram]{}
% \hyperref[s:AA]{Anexo A}

% \begin{figure}[H] % escoger H|h|t|b
% \centering
% \includegraphics[width=\textwidth]{diagrams/class_diagram.pdf}
% \caption{Diagrama de clases.}
% \label{fig:class_diagram}
% \end{figure}


Se exponen ahora algunos de los principales patrones aplicados en el diseño del software ~\cite{design_patterns}. 
En principio, el software que implementa el algoritmo BoW partía del prototipo Matlab, pero se ha 
realizado un diseño que procura un mejor mantenimiento y una fácil adaptabilidad a posibles cambios y
mejoras. Aunque la simplicidad aparente de la arquitectura \emph{pipe and filters}
invita a la escritura de
un código poco cuidado, se ha realizado un esfuerzo 
importante para obtener un resultado que se ajuste
a las prácticas de la ingeniería del software.

\begin{definitionlist}

 \item [Patrón Método Plantilla]
 Es un patrón de comportamiento que define el esqueleto de un algoritmo en forma de operaciones abstractas.
 El método plantilla fija la ordenación de las operaciones, pero permite que las subclases modifiquen su 
 comportamiento para adaptarse a la necesidades determinadas de cada momento. Es una técnica fundamental
 de reutilización de código ya que permite implementar las partes de un algoritmo que no cambian y
 dejar que sean las subclases quienes implementen el comportamiento que puede variar.
 
 La solución propuesta busca reutilizar código y establecer la ordenación de las operaciones
 llevadas a cabo. Para ello, la clase abstracta \emph{BoWSystem} fija la estructura del algoritmo propuesto
 e implementa algunas de las funciones comunes a la parte de training y de testing. 
 Esta clase es la clase raíz de la jerarquía de clases y la columna 
 vertebral que sostiene la 
 arquitectura \emph{pipes and filters} descrita. Su interfaz pública
 (el método \emph{execute}) como se puede ver en el Listado 
  \ref{cod:bowsystem.hpp} y \ref{cod:execute},
 ejecuta cada uno de los pasos a seguir. 

 \lstinputlisting[float,
language=C++,
caption={BoWSystem.hpp}, 
label=cod:bowsystem.hpp] 
{listings/BoWSystem.hpp}

 \lstinputlisting[
language=C++,
caption={Método \emph{execute}}, 
label=cod:execute] 
{listings/execute.cpp}


De esta clase derivan  dos: \emph{BoWTraining} y \emph{BoWPredict} que
implementan  los métodos  que las  diferencian, completando  el modelo
propuesto de Bag of Words:
 \begin{itemize}
  \item Método \emph{classify}. La parte de training crea el modelo
  SVM; la parte de testing obtiene una predicción a partir del modelo creado.
  \item Método \emph{obtain\_feature\_vectors\_reduct}. Para obtener
  el vector de características reducido 
  la parte de training en primer
  lugar realiza el \emph{clustering}. En segundo lugar obtiene el histograma. 
  La parte de testing sólo necesita obtener el vector de características reducido con los datos obtenidos durante el \emph{clustering}, esto es,
  la información relativa a los centroides.
 \end{itemize}
 
 Tomando como modelo esta implementación resultaría sencillo, por ejemplo, ampliar el sistema
 para leer los datos de la señal de ficheros o repositorios con otra estructura a los utilizados ahora
 sin necesidad de profundizar en el código. El módulo que se encarga de ello, aunque está implementado en la clase raíz, puede
 ser sobre escrito.
 
 \item [Patrón Strategy]
 Es un patrón de comportamiento que encapsula distintos algoritmos
 de una familia  y los hace
 intercambiables (ver Figura \ref{fig:strategy_example}). 
 Permite al algoritmo variar independientemente 
 de los clientes que lo usan.
 Captura la abstracción en una interfaz y oculta los 
 detalles de implementación en las clases
 derivadas de la interfaz.

 Se puede usar este patrón cuando se necesitan distintas 
 variantes de un algoritmo o muchas clases
 relacionadas difieren sólo en su comportamiento. 
 
 Este patrón se ha aplicado para construir el módulo de 
 \emph{clustering}. Así, se permite modificar el 
 algoritmo usado si se desea añadiendo un nuevo método. Para el
 sistema desarrollado se ha
 implementado el algoritmo \emph{k-means}. 

 
 \item [Patrón Fachada]
 
 La clase \emph{BoWSystem} sirve a su vez de fachada al proporcionar una interfaz unificada para el conjunto
 de interfaces, haciendo más fácil de usar el sistema. 
% En el siguiente listado se muestra un ejemplo de un
%  ¿cliente?
%  \lstset {language=C++}
%  \begin{lstlisting}
%   int main(int argc, char** argv)
%   {
%     vector<string> datasets_paths  =
% 		      {"data/Aset/", "data/Eset/"};
%     vector<int>    datasets_labels =
% 		      {  HEALTHY   ,  EPILEPTIC  };
% 		  
% 		  
%     // training
%     BoWSystem *BoWBonn = new BoWTraining(datasets_paths, datasets_labels);
%     BoWBonn->execute();
%   
%     // testing
%     datasets_paths = {"data/EEset/"};
%     BoWSystem *BoWTestBonn = new BoWPredictDataSet(datasets_paths);
%     BoWTestBonn->execute();
%   
%     // testing
%     datasets_paths = {"data/Aset/"};
%     BoWTestBonn = new BoWPredictDataSet(datasets_paths);
%     BoWTestBonn->execute();
%   }
%  \end{lstlisting}

 De forma similar a una clase \emph{Compilador}, ejemplo típico de un patrón fachada, que facilita la tarea de
 compilar sin ocultar la funcionalidad de más bajo nivel, la clase \emph{BoWSystem} abstrae de las 
 peculiaridades del sistema si no se quiere modificarlo para adaptarlo a nuevas necesidades.
 
%  [Explicar el polimorfismo y el patrón en la línea BoWSystem BowBonn...]
%  [Seguir explicando el diagrama de clases]
%  
%  
 
%  Figura \ref{fig:class_diagram}
% Figura \hyperref[fig:class_diagram]{}
% \hyperref[s:AA]{Anexo A}

\end{definitionlist}

 
 \begin{figure}[h] % escoger H|h|t|b
  \centering
  \includegraphics[width=0.55\textwidth]{/strategy.png}
  \caption{Ejemplo de patrón estrategia extraído del libro 
  \emph{Dessign Patterns Explained Simply}}
  \label{fig:strategy_example}
 \end{figure}
 

Todo ello da como resultado el diagrama de clases de la Figura
\ref{fig:class_diagram}
\begin{figure}[H] % escoger H|h|t|b
\centering
% \includegraphics[width=\textwidth]{/class_diagram_2.pdf}
\includegraphics[width=\textwidth]{/diagrama-1.png}
\caption{Diagrama de clases.}
\label{fig:class_diagram}
\end{figure}
% \end{definitionlist}


