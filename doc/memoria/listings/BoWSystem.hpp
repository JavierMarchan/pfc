 class BoWSystem{

 protected:  
  void virtual obtain_signal();
  void virtual obtain_feature_vectors_matrix();
  void virtual obtain_feature_vectors_reduct() = 0;
  void virtual classify() = 0;
  
 public:
  /*
   * Skeleton of the BoW algorithm
   */
  void execute();
  

 };