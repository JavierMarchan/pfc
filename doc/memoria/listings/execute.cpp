 void BoWSystem::execute()
 {
  {
    obtain_signal();
    obtain_feature_vectors_matrix();
    obtain_feature_vectors_reduct();
    classify();
  }
 }