#include "include/BoWPredictHeadSet.hpp"
#include "include/BoWSystem.hpp"

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main(int argc, char** argv)
{
  vector<double> freq            = {      128     };
  BoWSystem *BoWTestBonn = new BoWPredictHeadSet(freq);
  BoWTestBonn->execute();
}
