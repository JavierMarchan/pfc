#include "include/BoWTraining.hpp"
#include "include/BoWSystem.hpp"

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum condition{
  HEALTHY = 0,
  EPILEPTIC
};

int main(int argc, char** argv)
{
  vector<string> datasets_paths  = {"data/Aset/",
	 "data/Eset/"};
  vector<int>    datasets_labels = {  HEALTHY   ,
	   EPILEPTIC};
  vector<double> freqs           = {   173.61    , 
	   173.61};
  
  BoWSystem *BoWBonn = new BoWTraining(datasets_paths, datasets_labels, freqs);
  BoWBonn->execute();  
}
