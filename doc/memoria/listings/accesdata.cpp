 // Establish a connection to the EmoEngine
 if (EE_EngineConnect() == EDK_OK){
        // A handle to provide access to the EEG measurements
        DataHandle datah = EE_DataCreate();
        EE_DataSetBufferSizeInSec(4); //seconds
	EE_DataAcquisitionEnable();
	while(true){
                sleep(3); //wait 3 seconds
                 // Initiate retrieval of the latest EEG buffered data.
                 r = EE_DataUpdateHandle(datah);
		 // transfer the data into a buffer in our application
		 EE_DataGet(datah, channel, my_buffer_data);
         } //while        
}
 EE_DataFree(datah);
 EE_EngineDisconnect();
 EE_EmoEngineEventFree(eEvent);

    
