#include "include/BoWPredictDataSet.hpp"
#include "include/BoWSystem.hpp"

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum condition{
  HEALTHY = 0,
  EPILEPTIC
};

int main(int argc, char** argv)
{
  vector<string> datasets_paths  = {"data/Iset/"};
  vector<int>    datasets_labels = {  EPILEPTIC   };
  vector<double> freq            = {      256     };
  BoWSystem *BoWTestBonn = new BoWPredictDataSet(
		datasets_paths, datasets_labels, freq);
  BoWTestBonn->execute();
}

