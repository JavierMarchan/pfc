\chapter{Resultados}
\label{chap:resultados}

\drop{S}{e} ha desarrollado un  sistema para el diagnóstico automático
de  epilepsia.  A   partir  de  una  señal  \ac{EEG},   y  después  un
entrenamiento  supervisado,   el  sistema  clasifica   la  señal  como
\emph{sana} o \emph{epiléptica}.

A continuación se  dan más detalles de los datos  usados tanto para el
entrenamiento  como  para  el  \emph{testing}  y un  análisis  de  los
resultados y del tiempo consumido.

\section{\emph{Datasets}}

Tan importante como el desarrollo  del sistema es su fase posterior de
entrenamiento  y  de \emph{testing}.   Se  han  usado  un total  de  9
\emph{datasets} con distintas  características que corresponden a tres
fuentes diferentes.  La  gran mayoría de ellos se  pueden encontrar en
otros estudios  o investigaciones lo que ha  permitido una comparativa
de métodos directa, basada en  los resultados obtenidos del proceso de
clasificación. Hay  que destacar que  no todos los  \emph{datasets} se
encontraban  en  las mismas  unidades  de  medida,  al no  haber  sido
construidos con la misma instrumentación,  por lo que hubo que ajustar
los datos al hacer el \emph{testing} con las unidades de los datos con
los que se había hecho el entrenamiento.


\subsection{\emph{Training}}
\label{sec:training}

El sistema ha sido entrenado con los \emph{datasets} públicos
y usados en multitud de estudios de la
Universidad de Bonn ~\cite{bonn_datasets}. Se trata de cinco 
conjunto de datos (A, B, C, D y E), sin 
embargo solo se ha hecho uso de los conjuntos A y E que contienen
segmentos de 23.6 segundos de 100 canales distintos.
Estos segmentos fueron cuidadosamente 
seleccionados después de una inspección visual para eliminar
artefactos\footnote{Actividad muscular del usuario como movimientos
de los ojos, parpadeo o movimientos de las extremidades.}. El
conjunto A y B consisten 
en segmentos tomados de un \ac{EEG} superficial practicado
sobre cinco voluntarios sanos usando el sistema
internacional 10-20 de posicionamiento de los electrodos
superficiales (Figura \ref{fig:AB_bonn_electrodes}). Los 
voluntarios estaban despiertos y con los ojos abiertos (A) y
cerrados (B).

El conjunto E contiene segmentos con actividad ictal de cinco
pacientes que consiguieron un control completo de los ataques
después de la extirpación de una de las formaciones hipocampales,
que fue diagnosticada correctamente como la zona epileptogénica.

Todas las señales \ac{EEG} fueron registradas con una frecuencia
de muestreo de 173.61 Hz.


\begin{figure}[h] % escoger H|h|t|b
\centering
\includegraphics[width=0.45\textwidth]{/AB_bonn_electrodes.png}
\caption{Esquema de localización de los electrodos superficiales
acorde al sistema internacional 10-20}
\label{fig:AB_bonn_electrodes}
\end{figure}


\subsection{\emph{Testing}}
\label{sec:testing}

Para el \emph{testing} se han usado \emph{datasets} de tres fuentes
distintas. En primer lugar se han usado los mismos datos de la Universidad
de Bonn usados para el entrenamiento.
Además, se han usado otros dos que no han sido considerados durante
la etapa de entrenamiento: Uno de ellos, hecho público por el 
«Epilepsy Center of the University Hospital of Freiburg», con 
dos conjuntos nombrados como I y J. Ambos contienen datos 
extraídos de un ataque, libre de 
artefactos y muestreados a una frecuencia de 256 Hz.

Por último, un \emph{dataset} con registros que provienen de un
entorno no ideal como los anteriores, con numerosos artefactos y
atenuación. Proviene de el \ac{HRUM} y está compuesto de cinco 
conjuntos, denominados F, G, H y K para mantener la misma 
nomenclatura. El F registra la actividad de un paciente sano y
contiene multitud de artefactos. El G registra actividad 
inter-ictal y contiene también numerosos artefactos. El H contiene
registros de un ataque parcial en el lóbulo temporal izquierdo del 
cerebro. Contiene registros de la parte sana y epiléptica. El K registra una 
tónico-clónica\footnote{Es el tipo de crisis más conocido. En una
primera fase, llamada tónica, aparece rigidez muscular en el tronco
y las extremidades. En una segunda fase, llamada clónica, 
aparecen espasmos rítmicos de todo el cuerpo.}.
El F y G están muestreados a una frecuencia de 511.99 Hz. y el H y K
a una frecuencia de 200 Hz.
~\cite{freiburg_datasets}

\section{Repositorio}

Tanto el código como la documentación del proyecto se 
encuentran alojados en \emph{bitbucket.org}
\footnote{url del proyecto: \url{https://bitbucket.org/JavierMarchan/pfc}.}
~\cite{bitbucket} y se puede descargar ejecutando en un 
terminal:

\begin{listing}[language = python]
 git git@bitbucket.org:JavierMarchan/pfc.git
\end{listing}

Cuando se ha descargado el repositorio, se obtiene la estructura
del proyecto preparada para compilar. El \emph{Makefile}
(ver Apéndice \ref{chap:ap1_makefile}) hace una compilación 
modular, permitiendo ahorrar una gran cantidad de tiempo al
crear los distintos ejecutables o cuando se modifican uno
o varios ficheros. 
La estructura es la siguiente:
\begin{itemize}
 \item doc/ Contiene la documentación del proyecto preparada 
 para compilar ejecutando \emph{make} en un terminal.
 \item main/ Contiene el código del proyecto
 \begin{itemize}[noitemsep, label=$\triangleright$]
  \item bin/ Directorio donde se guardan los ejecutables.
  \item data/ Directorio donde se almacenan los \emph{datasets} y
  otros archivos generados durante la ejecución.
  \item include/ Contiene los archivos de cabecera.
  \item lib/ Contiene las librerías necesarias para compilar
  la parte que conecta con el hardware \ac{EEG} emotiv. Forma
  parte del \ac{SDK} y se obtienen al adquirir el producto.
  \item obj/ Contiene los ficheros \emph{objeto} generados 
  durante la compilación. 
 \end{itemize}
\end{itemize}


\section{Resultados}

Los distintos ejecutables que se crean son tres ejemplos
que muestran el uso del sistema:
\begin{itemize}
 \item \textbf{main-train} (Listado \ref{cod:main-train}). Se crea al ejecutar \emph{make main-train}.
 Se trata de un programa que hace uso de la parte de 
 \emph{training}. En él hay que indicar qué \emph{datasets}
 se van a usar para el entrenamiento, la \emph{etiqueta}
 de cada uno\footnote{Se utiliza una característica del 
 lenguaje, los enumerados, para crear dos etiquetas:
 HEALTHY y \\EPILEPTIC que representan a los \emph{datsets} 
 con una señal sana y epiléptica, respectivamente.} y la frecuencia 
 a la que fueron registrados los datos. Aunque la ruta de los
 \emph{datasets} puede ser cualquiera, se ha creado un 
 directorio \emph{data} especialmente para guardarlos en él.
 En este directorio se crean dos ficheros como resultado 
 de la ejecución del \emph{training}:
 \begin{itemize}
  \item [-] \texttt{centroids.csv}. Como su nombre indica, 
  contiene las coordenadas de los centroides producidos en el 
  \emph{training}. Es un fichero en formato .csv que se puede ver
  con cualquier editor de texto. 
  \item [-] \texttt{model}. Es el modelo generado por el 
  clasificador en el \emph{training}. Al ser texto plano se 
  puede ver con cualquier editor de texto.
 \end{itemize}
 
 Los dos ficheros serán utilizados posteriormente en la fase 
 de  \emph{testing}

 \item \textbf{main-predict} (Listado \ref{cod:main-predict}). Se crea al ejecutar 
 \emph{make main-predict}. Se trata de un programa que hace uso de
 la parte de \emph{testing}. En el ejemplo mostrado se ejecuta
 con el \emph{dataset} I, que fue creado tomando registros a una
 frecuencia de 256 Hz. También se observa que se incluye la
 variable \emph{dataset\_label} y se le asigna el valor  
 \emph{EPILEPTIC}. Esto se hace exclusivamente para obtener un
 porcentaje de aciertos en la predicción, ya que conocemos que
 el \emph{dataset} que estamos probando le corresponde dicha 
 etiqueta. Por supuesto, en un entorno real no sólo careceríamos
 de esta información, sino que el propio objetivo sería obtener
 la predicción.
 
 \item \textbf{main-headset} (Listado \ref{cod:main-headset}). Se crea al ejecutar 
 \emph{make main-headset}. Se trata de un programa que hace uso de
 la parte de \emph{testing}, pero conectando
 con el hardware \ac{EEG} \emph{emotiv} descrito. Mientras se
 está ejecutando, se registran las mediciones procedentes de los 
 16 canales. Cuando el usuario decide terminar de recoger
 datos, se usan estos como entrada al 
 sistema. Además, se crea un nuevo \emph{dataset} en el directorio
 \emph{data} llamado \emph{Headset} que almacena los datos.
 


 \lstinputlisting[float,
language=C++,
caption={main-train}, 
label=cod:main-train]
{listings/main-train.cpp}

 \lstinputlisting[float,
language=C++, 
caption={main-predict}, 
label=cod:main-predict]
{listings/main-predict.cpp}

 \lstinputlisting[float,
language=C++, 
caption={main-predict}, 
label=cod:main-headset]
{listings/main-headset.cpp}

\end{itemize}


\section{Análisis de los resultados}

La Tabla \ref{tab:results_01} muestra los resultados que se obtienen
realizando el \emph{training} y el \emph{testing}
como se describe en las secciones \ref{sec:training} y 
\ref{sec:testing}, respectivamente.

\begin{table}[htbp]
  \centering
  \input{tables/results_01.tex}
  \caption{Resultados}
  \label{tab:results_01}
\end{table}

Se pueden observar varias cosas interesantes de la tabla. En
primer lugar, los resultados de la clasificación de 
los \emph{datasets} de la Universidad
de Bonn ~\cite{bonn_datasets} son de un \textbf{100 \% de aciertos}. En 
la Tabla \ref{tab:comparative}
se puede ver una comparativa con los resultados obtenidos en 9
trabajos que usan los mismos \emph{datasets} de la Universidad
de Bonn pero con 
diferentes métodos que van desde las redes neuronales hasta el 
análisis Wavelet.

%aquí la tabla 5 de paper esi
% \begin{table}[htbp]
%   \centering
%   \input{tables/comparative.tex}
%   \caption{Comparativa con otros trabajos}
%   \label{tab:comparative}
% \end{table}
% 

\begin{table}[htbp]
  \centering
  \input{tables/comparative.tex}
  \caption{Comparativa con otros trabajos}
  \label{tab:comparative}
\end{table}

En el trabajo actual se mejoran todos los anteriores.

En segundo lugar hay que destacar la calidad de los resultados 
en el resto de \emph{datasets}, más complejos y realistas por 
la presencia de ruidos y artefactos en ellos.

Estos resultados son coherentes con el trabajo ~\cite{paper_esi}, 
punto de partida de este \ac{PFC}. En el citado trabajo se 
realizan más experimentos, pero si se presta atención al 
realizado usando un \emph{kernel} \ac{RBF} para el clasificador como
el utilizado aquí, se puede ver como algunos \emph{datasets} bajan
su porcentaje de acierto (especialmente el F), otros suben y, 
globalmente, mejora ligeramente el resultado. Las diferencias en
los resultados, más pequeñas de lo esperado en el planteamiento
del \ac{PFC}, se pueden deber al uso de librerías diferentes 
(se ha desarrollado un sistema en C++ frente a un prototipo en Matlab)
y a usar un algoritmo distinto en la etapa de \emph{clustering}: 
\emph{k-means} en este trabajo frente a \ac{EM} en ~\cite{paper_esi}. 
Ver Cuadro \ref{tab:comparative_esi}.


En cuanto al caso del \emph{dataset} F, aunque no es favorable,
no hay que perder de vista que en el estado del arte no existen
experimentos con datos de estas características, extraídos de un
entorno real con multitud de artefactos e interferencias.

\begin{table}[htbp]
  \centering
  \input{tables/comparative_esi.tex}
  \caption{Comparativa con los datos de ~\cite{paper_esi}}
  \label{tab:comparative_esi}
\end{table}


\section{Análisis de tiempo}

Aunque el tiempo consumido en realizar el 
\emph{training} y \emph{testing} no 
es algo crítico, se ha realizado un análisis del tiempo por
cada \emph{dataset} (Tabla \ref{tab:tiempos_testing}) y del tiempo
que consume el entrenamiento (Tabla \ref{tab:tiempos_training}).

Para medir los tiempos se ha usado la librería \emph{chrono} 
que pertenece al estándar C++11 y que está definida en el
archivo de cabecera \emph{chrono}


\begin{table}[h]
  \centering
  \input{tables/tiempos_testing.tex}
  \caption{Tiempo consumido durante el \emph{testing}
  por cada \emph{dataset}}
  \label{tab:tiempos_testing}
\end{table}

Los tiempos corresponden a la ejecución del sistema sobre
un procesador Intel® Core™ i5-2450M
\begin{table}[h]
  \centering
  \input{tables/tiempos_training.tex}
  \caption{Tiempo consumido durante el \emph{training}}
  \label{tab:tiempos_training}
\end{table}
